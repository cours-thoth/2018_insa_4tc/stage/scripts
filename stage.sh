#!/bin/sh
sql_database_location=${HOME}/Documents/Stage/stage.sqlite3
internship_file_location=${HOME}/Documents/Stage/internships
cache_location=${HOME}/.cache

function help {
    echo "TODO: write help";
}

function database_init {
    if [ -e ${sql_database_location} ]; then
        echo "the database already exists"
        exit -1
    fi


    # TODO: add verification if "name" exists in "entreprises" table for insertion in two other tables
    sqlite3 ${sql_database_location} << EOS
        create table Internships(
            name varchar(100) primary key
        );

        create table Statuses(
            status varchar(100) primary key
        );

        insert into Statuses (status) values ('abandonned');
        insert into Statuses (status) values ('not_reviewed');
        insert into Statuses (status) values ('in_progress');
        insert into Statuses (status) values ('motivation_letter_sent');
        insert into Statuses (status) values ('second_letter_send');
        insert into Statuses (status) values ('meeting_planned');
        insert into Statuses (status) values ('accepted') ;




        create table Status (
            name varchar(100),
            status varchar(100) default 'not_treated',
            note text,
            date datetime default (datetime('now','localtime')),
            primary key (name, date),
            foreign key (status) references Statuses(status)
        );

        create table Annotations (
            name varchar(100),
            annotation text,
            primary key (name, annotation)
        );
EOS
}

function database_entreprise_exists {
    sqlite3 ${sql_database_location} << EOS

EOS
}

function database_add {
    sqlite3 ${sql_database_location} << EOS
        insert into Internships ( name ) values ( '$1' );
EOS
}

function database_list_companies {
    sqlite3 ${sql_database_location} << EOS
        select * from Internships;
EOS
}

function database_list {
    if [ "$1" = "all" ]; then
        sqlite3 ${sql_database_location} << EOS
        select Internships.name, MaxDate.status, MaxDate.date
        from Internships
        left join (
            select name, status, max(date) as date
            from Status
            group by name
        ) as Maxdate
        on MaxDate.name = Internships.name;
EOS
    else
        sqlite3 ${sql_database_location} << EOS
        select Internships.name, MaxDate.status, MaxDate.date
        from Internships
        left join (
            select name, status, max(date) as date
            from Status
            group by name
        ) as Maxdate
        on MaxDate.name = Internships.name
        where status is not 'refused'
        and status is not 'abandonned';
EOS
    fi

}

function database_ls {
    sqlite3 ${sql_database_location} << EOS
    select name
    from Internships
EOS
}



function database_add_status {
    sqlite3 ${sql_database_location} << EOS
        insert
            into Status (name, status, note)
            values ('$1', '$2', '$3')
        ;
EOS
}

function database_save {
    mkdir -p ${cache_location}/stage_databases
    save_location="${cache_location}/stage_databases/$(date '+%Y-%m-%d_%H-%M').$$.sqlite3"
    cp ${sql_database_location} ${save_location}
    echo "database saved at ${save_location}"
}

function taskwarrior_add {
    if [ -z "$1" ]; then
        echo "name of internship required"
        exit -1
    fi

    DOCUMENT="${HOME}/Documents/Stage/entreprises/$1.md"

    task add project:stage2019.${1} "résumer domaine de l'entreprise et contenu du stage ( si non spontané ), lettre motiv, envoyer et marquer date" due:+1day +$1
    touch ${DOCUMENT}
    task add project:stage2019.${1} "relancer 10 jours après" due:+11days +$1
    task +LATEST annotate ${DOCUMENT}
}

function add {
    taskwarrior_add $1
    database_add $1
}

function status {
    if [ -z "$1" ]; then
        echo "name of stage required"
        exit -1
    fi

    if [ -z "$2" ]; then
        sqlite3 ${sql_database_location} << EOS
            select *
                from Status
                where name= "$1"
            ;
EOS
    exit 0

    fi

    case $2 in
        motivation | motivation_sent)
            database_add_status $1 motivation_sent "$3"
            # todo status done in taskwarrior
            ;;

        abandon | abandonned )
            database_add_status $1 'abandonned' "$3"
            ;;

        refused | refusé )
            database_add_status $1 'refused' "$3"
            ;;

        meet | meeting_planned | rdv )
            database_add_status $1 'meeting_planned' "$3"
            ;;

        *)
            echo "this status don't exist or implementation not done yet"
            ;;
    esac
}



# TODO: verification of second parameter for add_db_only

case $1 in
    init)
        database_init
        ;;

    add_db_only)
        database_add $2
        ;;

    ls)
        database_ls
        ;;

    list)
        database_list $2
        ;;

    add)
        add $2
        ;;

    edit)
        shift
        to_edit=
        for i in $@
        do
            to_edit="${to_edit} ${internship_file_location}/$i.md"
        done
        $EDITOR $to_edit
        ;;

    status)
        status $2 $3 "$4" # TODO: status advancement of project ( and update in taskwarrior as well) store date and allow to use comments for each stage ( in $4 )
        ;;

    save)
        database_save
        ;;

    help)
        help
        ;;
    *)
        help
        ;;
esac
