autocmd FileType sh unlet b:current_syntax
autocmd FileType sh sy include @sql syntax/sql.vim
autocmd FileType sh syn region sqlSnip matchgroup=Snip start=/<< EOS$/ end=/^EOS$/ contains=@sql containedin=ALL
autocmd FileType sh hi link Snip SpecialComment
