todo
----

### functions
- [x] rename to internship or put it all in french ( NOTE: renamed variables to internship, but keeped name of the script as "stage" )
- [x] when `ls` : show latest status for each internship
- [x] add save function to create a copy of the database
- [ ] mark command to add a status and a status note
    - [x] basic statuses
    - [ ]-> when some marks are called, trigger taskwarrior
- [ ] add notes and allow to open website for each company ( don't use taskwarrior for links anymore -> use it only for tasks )
- [ ] create database if not existing
- [ ] multiple internship propositions in the same company :
    - workaround : put a dot :derp:
- [ ] zsh completion -> then create papis completion tool

### due

- [ ] list : show dates of sending ( or/and date when I need to contact again )
- [ ] show : show detail for specific

### later
- [ ] verify argument number
- [ ] verify well-formed arguments
- [ ] colors =)
- [ ] configuration file to set the different folders
